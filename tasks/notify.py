import httpx

import schemas
from config import TELEGRAM_BOT_TOKEN, TELEGRAM_CHAT_ID
from .context import Context


__all__ = (
    'notify_new_user',
)


_url = f'https://api.telegram.org/bot{TELEGRAM_BOT_TOKEN}/sendMessage'


async def send_message(text: str):
    async with httpx.AsyncClient() as client:
        return await client.get(_url, params={
            'chat_id': TELEGRAM_CHAT_ID,
            'text': text,
        })


async def notify_new_user(ctx: Context, user_id: schemas.ID, email: str, account_type: schemas.AccountType):
    types = {
        schemas.AccountType.customer: '\U0001f64b\U0001f3fb\U0000200d\U00002642\U0000fe0f',
        schemas.AccountType.celebrity: '\U0001f64b\U0000200d\U00002640\U0000fe0f',
        schemas.AccountType.admin: '\U0001f468\U0000200d\U0001f4bb',
    }

    emoji = types.get(account_type, '')

    print('account_type', account_type, emoji)

    text = f'{emoji} {email}'

    return await send_message(text)
