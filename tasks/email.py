import logging

from config import EMAIL_FROM_ADDR, EMAIL_FROM_NAME, BASE_URL
from services.sendgrid import sendgrid, From, To
from .context import Context
import crud
import schemas
from auth import create_autologin_url


async def email_unread_message_celebrity(ctx: Context, user_id: schemas.ID, email: str, sender: str, receiver: str, chat_url: str, avatar_url: str):
    url = create_autologin_url(user_id, chat_url)

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=To(email),
            template_id='d-41c7094473a449588fef30c2256385c0',
            template_data={
                'user_who_sent': sender,
                'user_who_receive': receiver,
                'chat_url': url,
                'avatar_url': avatar_url,
            },
        )


async def email_unread_message_fan(ctx: Context, user_id: schemas.ID, email: str, sender: str, receiver: str, chat_url: str, avatar_url: str):
    url = create_autologin_url(user_id, chat_url)

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=email,
            template_id='d-41c7094473a449588fef30c2256385c0',
            template_data={
                'user_who_sent': sender,
                'user_who_receive': receiver,
                'chat_url': url,
                'avatar_url': avatar_url,
            },
        )


async def email_fan_no_messages(ctx: Context, fan_id: schemas.ID, celebrity_id: schemas.ID):
    result = await crud.user_has_messages(fan_id)

    if result:
        return False, f'User {fan_id} has messages'

    try:
        fan = await crud.get_base_profile(fan_id)
        celebrity = await crud.get_base_profile(celebrity_id)
    except crud.DoesNotExist:
        return False, 'User does not exist'

    avatar_url = celebrity['avatar']['thumbnails']['sm']['url'] if celebrity['avatar'] else ''
    chat_url = create_autologin_url(fan_id, '/' + celebrity['username'])

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=fan['email'],
            template_id='d-15923f74411a4dafb5e0001b46005495',
            template_data={
                'creatorname': celebrity['name'] or celebrity['username'],
                'chat_url': chat_url,
                'avatar_url': avatar_url,
            },
        )


async def email_verify(ctx: Context, user_id: schemas.ID, email: str, name: str, token: str):
    url = f'{BASE_URL}/api/auth/verify/email/{token}'

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=email,
            template_id='d-f8c7b64fdf94441799cc1e33c29ec842',
            template_data={
                'name': name,
                'link': url,
            },
        )


async def email_celebrity_registered(ctx: Context, user_id: schemas.ID, email: str):
    url = create_autologin_url(user_id, '/profile')

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=email,
            template_id='d-e8ec942d238542c98c0df7e409bc6a99',
            template_data={
                'login_url': url,
            },
        )


async def email_fan_registered(ctx: Context, user_id: schemas.ID, email: str):
    url = create_autologin_url(user_id, '/profile')

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=email,
            template_id='d-8eee6ba9c06443dd9875f6eb91421de6',
            template_data={
                'login_url': url,
            },
        )


async def email_password_changed(ctx: Context, user_id: schemas.ID, email: str):
    login_url = create_autologin_url(user_id, '/profile')

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=email,
            template_id='d-ebd52b1d18604783b22551387a8851ea',
            template_data={
                'login_url': login_url,
            },
        )


async def email_payment_profile_verified(ctx: Context, user_id: schemas.ID, email: str, first_name: str):
    login_url = create_autologin_url(user_id, '/profile/payment-profile')

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=email,
            template_id='d-72104242dde54dd1a4591ee28efcab79',
            template_data={
                'first_name': first_name,
                'login_url': login_url,
            },
        )


async def email_read_but_not_replied(ctx: Context, to_id: schemas.ID, from_id: schemas.ID, chat_id: schemas.ID, last_message_id: schemas.ID):
    to_user = await crud.get_base_profile(to_id)
    from_user = await crud.get_base_profile(from_id)

    result = await crud.user_has_replies(to_id, chat_id, last_message_id)

    if result:
        return False

    if not to_user:
        return

    if not from_user:
        return False, f'User {from_id} not found'

    chat_url = create_autologin_url(to_id, '/' + from_user['username'])

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=to_user['email'],
            template_id='d-41e197efc1a042ffa5d9f8051a630721',
            template_data={
                'chat_url': chat_url,
            },
        )


async def email_celebrity_share_link(ctx: Context, user_id: schemas.ID):
    count = await crud.count_user_chats(user_id)

    if count > 0:
        return False, f'User has {count} chats'

    try:
        user = await crud.get_user_by_id(user_id)
    except crud.DoesNotExist:
        return False, f'User {user_id} does not exist'

    url = create_autologin_url(user_id, '/profile')

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=user.email,
            template_id='d-a0de92e3f0fa46488098c2619886fd2f',
            template_data={
                'creatorname': user.name or user.email,
                'profile_url': url,
            },
        )


async def email_celebrity_tip_received(ctx: Context, to_id: schemas.ID, from_id: schemas.ID):
    user = await crud.get_base_profile(to_id)
    from_user = await crud.get_base_profile(from_id)

    chat_url = create_autologin_url(to_id, '/' + from_user['username'])

    async with sendgrid as client:
        return await client.send_template(
            from_email=From(EMAIL_FROM_ADDR, EMAIL_FROM_NAME),
            to_email=user['email'],
            template_id='d-777c494f03654db79d2fb390ddb970a1',
            template_data={
                'creatorname': user['name'] or user['email'],
                'chat_url': chat_url,
            },
        )

