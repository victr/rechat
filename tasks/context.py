from datetime import datetime
from typing import TypedDict

from arq import ArqRedis


__all__ = (
    'Context',
)


class Context(TypedDict):
    redis: ArqRedis
    job_id: str
    job_try: int
    score: int
    enqueue_time: datetime
