from arq.connections import RedisSettings

import models
from config import REDIS_HOST
from .context import Context
from tasks import email, notify


async def startup(ctx: Context):
    await models.startup()


async def shutdown(ctx: Context):
    await models.shutdown()


class WorkerSettings:

    redis_settings = RedisSettings(host=REDIS_HOST)

    on_startup = startup
    on_shutdown = shutdown

    functions = [
        email.email_fan_no_messages,
        email.email_unread_message_fan,
        email.email_unread_message_celebrity,
        email.email_verify,
        email.email_celebrity_registered,
        email.email_fan_registered,
        email.email_password_changed,
        email.email_payment_profile_verified,
        email.email_read_but_not_replied,
        email.email_celebrity_share_link,
        email.email_celebrity_tip_received,
        notify.notify_new_user,
    ]
