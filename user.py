from datetime import timedelta

from fastapi import APIRouter, Depends, HTTPException, status, Request, Form, File

import models
import crud
import auth
import files
from errors import Error
from schemas import *
from config import DOCUMENT_THUMBNAILS_SIZES
from depends import RedisConnection
import services.mail


router = APIRouter()


@router.get('/me', response_model=TokenData)
async def me(token_data: auth.Token = Depends()):
    return token_data


@router.post('/check')
async def check_username(request: CheckUsernameRequest, token_data: auth.Token = Depends()):
    result = await crud.check_username(token_data.id, request.username)

    return result


@router.post('/country')
async def check_username(request: Request):
    return request.headers.get('cf-ipcountry', 'US')


@router.get('/fan/profile', response_model=FanProfile)
async def fan_profile(token_data: auth.Token = Depends()):
    result = await crud.get_fan_profile(token_data.id)

    if not result:
        raise HTTPException(status.HTTP_404_NOT_FOUND, Error.ProfileNotFound.value)

    return result


@router.post('/fan/profile', response_model=FanProfile)
async def fan_profile_update(profile: FanProfileCreate, token_data: auth.Token = Depends()):
    result = await crud.update_fan_profile(token_data.id, profile)

    if not result:
        raise HTTPException(status.HTTP_404_NOT_FOUND, Error.ProfileNotFound.value)

    return await crud.get_fan_profile(token_data.id)


@router.get('/celebrity/profile', response_model=CelebrityProfile)
async def celebrity_profile(token_data: auth.Token = Depends()):
    result = await crud.get_celebrity_profile(token_data.id)

    if not result:
        raise HTTPException(status.HTTP_404_NOT_FOUND, Error.ProfileNotFound.value)

    return result


@router.post('/celebrity/profile')
async def celebrity_profile_update(profile: CelebrityProfileCreate, token_data: auth.Token = Depends(), redis: RedisConnection = Depends()):
    data = await crud.get_base_profile(token_data.id)

    if data is None:
        raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')

    user = BaseProfile(**data)

    email = profile.email or user.email

    if profile.onboarding == 1 and email:
        token = auth.encode_token({
            'id': str(token_data.id),
            'email': email,
        }, expires_delta=timedelta(days=7))

        await redis.task(
            'email_verify',
            user_id=token_data.id,
            email=email,
            name=profile.name or user.username,
            token=token,
        )

    try:
        result = await crud.update_celebrity_profile(token_data.id, profile)
    except models.UniqueViolationError:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, Error.UserExists.value)

    if not result:
        raise HTTPException(status.HTTP_404_NOT_FOUND, Error.ProfileNotFound.value)

    if profile.onboarding == 2 and user.username:
        await crud.init_celebrity_chats(token_data.id, user)

    return await crud.get_celebrity_profile(token_data.id)


@router.get('/celebrity/profile/payment')
async def celebrity_payment_profile(token: auth.Token = Depends()):
    if not token.account_type.is_celebrity():
        raise HTTPException(status.HTTP_403_FORBIDDEN)

    profile = await crud.get_celebrity_payment_profile(token.id)

    if not profile:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    return profile


@router.post('/celebrity/profile/payment')
async def celebrity_payment_profile_update(
    first_name: str = Form(...),
    last_name: str = Form(...),
    birthday: str = Form(...),
    country: str = Form(...),
    city: str = Form(...),
    state: str = Form(...),
    address: str = Form(...),
    zip: str = Form(...),
    resident: ResidentType = Form(...),
    document_type: DocumentType = Form(...),
    document_front: files.PhotoFile = File(...),
    document_back: files.PhotoFile = File(None),
    token: auth.Token = Depends(),
):
    if not token.account_type.is_celebrity():
        raise HTTPException(status.HTTP_403_FORBIDDEN)

    profile = await crud.get_celebrity_payment_profile(token.id)
    email = await crud.get_user_email(token.id)

    if profile and profile.status in (PaymentProfileStatus.moderation, PaymentProfileStatus.approved):
        raise HTTPException(status.HTTP_403_FORBIDDEN)   

    try:
        date = datetime.strptime(birthday[:10], '%Y-%m-%d')

        file_document_front = await files.save_image(document_front.file, DOCUMENT_THUMBNAILS_SIZES)
        file_document_back = await files.save_image(document_back.file, DOCUMENT_THUMBNAILS_SIZES)

        async with models.database.transaction():
            file_document_front.uploaded_by = token.id
            await crud.create_file(file_document_front)

            file_document_back.uploaded_by = token.id
            await crud.create_file(file_document_back)

            await crud.update_celebrity_payment_profile(PaymentProfileCreate(
                id=token.id,
                user_id=token.id,
                status=PaymentProfileStatus.moderation,
                first_name=first_name,
                last_name=last_name,
                state=state,
                city=city,
                zip=zip,
                address=address,
                birthday=date,
                country=country,
                resident=resident,
                document_type=document_type,
                document_front=file_document_front.id,
                document_back=file_document_back.id,
            ))

            await services.mail.payment_profile_created(
                email=email,
                first_name=first_name,
            )

    except models.UniqueViolationError:
        raise HTTPException(status.HTTP_400_BAD_REQUEST)

    return await crud.get_celebrity_payment_profile(token.id)


@router.get('/profile/media')
async def get_profile_media(user_id: ID):
    return await crud.get_profile_media(user_id)


@router.post('/profile/media')
async def set_profile_media(request: SetProfileMediaRequest, token: auth.Token = Depends()):
    await crud.set_profile_media(token.id, request.ids)

    return await crud.get_profile_media(token.id)
