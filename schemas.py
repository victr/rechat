import uuid
import enum
from decimal import Decimal
from datetime import datetime
from typing import Optional, Any, List, Dict

from pydantic import validator, constr, Field, BaseModel, UUID4, EmailStr, PositiveInt

from config import MESSAGE_MAX_TEXT_LENGTH


class ID(UUID4):

    @classmethod
    def generate(cls):
        return uuid.uuid4()


class AccountType(enum.IntEnum):
    admin = 0
    customer = 1
    celebrity = 2

    def is_admin(self):
        return self is self.admin

    def is_fan(self):
        return self is self.customer

    def is_celebrity(self):
        return self is self.celebrity


class AccountCategory(enum.IntEnum):
    other = 0
    actress = 1
    blogger = 2


class Gender(enum.IntEnum):
    male = 1
    female = 2


class ResidentType(enum.IntEnum):
    none = 0
    us = 1
    ca = 2


class DocumentType(enum.IntEnum):
    passport = 1
    drivers_license = 2
    id = 3


class PaymentProfileStatus(enum.IntEnum):
    moderation = 1
    declined = 2
    approved = 3


class Token(BaseModel):
    access_token: str
    token_type: str = 'bearer'


class TokenData(BaseModel):
    id: ID
    account_type: AccountType


class AutologinToken(BaseModel):
    user_id: ID = Field(alias='id')
    redirect_url: str = Field(alias='u')


class AuthorizationUrl(BaseModel):
    authorization_url: str


class LoginOtpResponse(BaseModel):
    new_user: bool
    token: Token


class FileID(BaseModel):
    file_id: str


class Thumbnail(BaseModel):
    width: int
    height: int
    size: int
    url: str


class MessageType(enum.IntEnum):
    text = 1
    photo = 2
    video = 3
    audio = 4
    voice = 5
    note = 6
    document = 7
    tip = 8


class MessagePhotoData(BaseModel):
    thumbnails: Dict[str, Thumbnail]


class Username(BaseModel):
    username: str

    @validator('username')
    def validate_username(cls, v):
        if len(v) < 5:
            raise ValueError('A username must have at least 5 characters')

        if v[0].isnumeric():
                raise ValueError('A username must start with a letter')

        return v


class UserUpdate(BaseModel):
    name: Optional[str] = None
    username: Optional[str] = None
    photo: Optional[ID] = None
    gender: Optional[Gender] = None
    birthday: Optional[datetime] = None
    phone: Optional[str] = None
    phone_verified: Optional[bool] = False
    email: Optional[str] = None
    email_verified: Optional[bool] = False
    email_notification: Optional[bool] = True
    ip_address: Optional[str] = None
    ip_country: Optional[str] = None
    last_login_at: Optional[datetime] = None

    @validator('phone')
    def validate_phone(cls, v):
        if type(v) is str:
            v = ''.join(filter(str.isdigit, v))

        return v

    @validator('birthday')
    def validate_birthday(cls, v):
        if type(v) is datetime:
            v = v.replace(
                tzinfo=None,
                hour=0,
                minute=0,
                second=0,
            )

        return v


class UserCreate(UserUpdate):
    id: ID = Field(default_factory=ID.generate)
    email: Optional[EmailStr]
    phone: Optional[str]
    hashed_password: Optional[str] = None
    is_active: bool
    is_superuser: bool
    created_at: datetime = Field(default_factory=datetime.utcnow)
    account_type: AccountType
    invite: Optional[ID]


class BaseProfile(BaseModel):
    id: ID
    name: Optional[str]
    username: Optional[str]
    email: Optional[str]
    email_verified: bool
    account_type: AccountType
    avatar: Optional[MessagePhotoData]


class UserLogin(BaseModel):
    email: EmailStr
    password: str


class UserRegister(BaseModel):
    email: EmailStr
    password: constr(min_length=6)
    account_type: AccountType
    invite: Optional[str]


class UserStatus(BaseModel):
    status: int = 0
    last_seen: int = 0


class OtpCodeRequest(BaseModel):
    phone: constr(strip_whitespace=True, min_length=10, max_length=20)
    code: Optional[str]
    invite: Optional[str]
    account_type: Optional[AccountType]

    @validator('phone')
    def phone_number(cls, v):
        return ''.join(filter(str.isdigit, v))


class CelebrityChatInfo(BaseModel):
    id: ID
    name: Optional[str]
    username: str
    greeting: str
    greeting_video: Optional[ID]
    price: Decimal
    bio: Optional[str]
    status: Optional[UserStatus]
    location: Optional[str]
    photo: Optional[ID]
    avatar: Optional[MessagePhotoData]


class FanChatInfo(BaseModel):
    id: ID
    name: Optional[str]
    username: str
    bio: Optional[str]
    status: Optional[UserStatus]
    photo: Optional[ID]
    avatar: Optional[MessagePhotoData]


class OAuth2State(BaseModel):
    aud: str
    account_type: AccountType
    exp: int = 0
    redirect_uri: str


class OAuthAccount(BaseModel):
    id: ID = Field(default_factory=ID.generate)
    user_id: Optional[ID] = None
    name: str
    username: Optional[str] = None
    account_email: EmailStr
    account_id: str
    access_token: str
    expires_at: datetime
    refresh_token: Optional[str] = None


class ProfileMedia(BaseModel):
    id: ID
    thumbnails: Dict[str, Thumbnail]


class FileCreate(BaseModel):
    id: ID = Field(default_factory=ID.generate)
    size: int
    url: constr(max_length=2048)
    mime: Optional[str]
    uploaded_at: datetime = Field(default_factory=datetime.now)
    uploaded_by: Optional[ID]
    meta: Any = None


class ProfileSocial(BaseModel):
    website: Optional[str]
    facebook: Optional[str]
    instagram: Optional[str]
    twitter: Optional[str]
    snapchat: Optional[str]
    google: Optional[str]


class ProfileBase(UserUpdate):
    bio: constr(max_length=500, strip_whitespace=True) = ''

    @classmethod
    def create_profile(cls, account_type: AccountType):
        return {
            AccountType.celebrity: CelebrityProfileCreate,
            AccountType.customer: FanProfileCreate,
        }.get(account_type, cls)()


class CelebrityProfileCreate(ProfileBase, ProfileSocial):
    category: Optional[AccountCategory] = AccountCategory.other
    latitude: Optional[float]
    longitude: Optional[float]
    place_id: constr(max_length=100) = ''
    location: constr(max_length=200) = ''
    greeting: constr(max_length=100, min_length=1) = None
    greeting_video: Optional[ID] = None
    gender: Optional[Gender]
    price: float = 0.1
    onboarding: Optional[int]
    media: Optional[List[ProfileMedia]] = None
    free_messages: Optional[int] = None
    free_messages_enabled: Optional[bool] = None

    @validator('price')
    def validate_price(cls, v):
        if v is not None and not 0.1 < v < 9.9:
            if v < 0.1:
                raise ValueError('Price is lower than min price (1.0)')
            elif v > 9.9:
                raise ValueError('Price is higher than max price (9.9)')

        return v

    @validator('media')
    def validate_media(cls, v):
        if len(v) > 5:
            raise ValueError('Limit exceeded')

        return v

    @validator('free_messages')
    def validate_free_messages(cls, v):
        if 0 < v <= 5:
            return v

        raise ValueError('Value must be between 0 and 5')


class CelebrityProfile(CelebrityProfileCreate):
    avatar: Optional[MessagePhotoData] = None


class FanProfileCreate(ProfileBase, ProfileSocial):
    pass


class FanProfile(FanProfileCreate):
    avatar: Optional[MessagePhotoData] = None


class ChatCreate(BaseModel):
    id: ID = Field(default_factory=ID.generate)
    created_at: datetime = Field(default_factory=datetime.utcnow)


class Chat(ChatCreate):
    name: str


class ChatUsersCreate(BaseModel):
    id: ID = Field(default_factory=ID.generate)
    chat_id: ID
    user_id: ID
    with_id: ID


class BalanceCharge(BaseModel):
    value: Decimal


class ChatStart(BaseModel):
    user_id: ID


class VideoSource(BaseModel):
    url: str
    size: int
    duration: float
    format: str
    width: int
    height: int
    bit_rate: int
    codec_tag: str
    content_type: str
    has_audio: bool
    audio_codec: Optional[str] = None


class AudioSource(BaseModel):
    url: str
    size: int
    duration: float
    format: str
    bit_rate: int
    content_type: str


class MessageVideoData(BaseModel):
    thumbnails: Dict[str, Thumbnail]
    sources: Dict[str, VideoSource]


class MessageVoiceData(BaseModel):
    sources: Dict[str, AudioSource]


class MessageTipData(BaseModel):
    credits: PositiveInt
    amount: Optional[float] = 0.0


class Message(BaseModel):
    id: ID = Field(default_factory=ID.generate)
    type: MessageType
    created_at: datetime = Field(default_factory=datetime.utcnow)
    chat_id: ID
    file_id: Optional[ID]
    sender_id: ID
    text: constr(max_length=MESSAGE_MAX_TEXT_LENGTH)
    is_deleted: bool = False
    is_read: bool = False
    data: Any = None


class MessageInput(BaseModel):
    id: ID = Field(default_factory=ID.generate)
    type: MessageType
    created_at: Optional[datetime]
    chat_id: ID
    sender_id: Optional[ID]
    file_id: Optional[ID]
    text: constr(max_length=MESSAGE_MAX_TEXT_LENGTH)
    data: Any = None


class ChatAction(BaseModel):
    chat_id: ID
    type: str


class MessageUpdate(BaseModel):
    id: ID
    chat_id: ID
    is_read: Optional[bool]
    is_edited: Optional[bool]


class ChatSearchRequest(BaseModel):
    query: str


class MessageListActionRequest(BaseModel):
    chat_id: ID
    messages: List[ID]


class ChatListActionRequest(BaseModel):
    chats: List[ID]


class UserStatusRequest(BaseModel):
    chats: Dict[ID, ID]


class GetMessagesRequest(BaseModel):
    chat_id: ID
    types: Optional[List[MessageType]] = None


class GetChatsRequest(BaseModel):
    chats: List[ID] = []


class Balance(BaseModel):
    id: ID
    user_id: ID
    value: Decimal
    pending: Decimal = Decimal('0')
    available: Decimal = Decimal('0')
    waiting: Decimal = Decimal('0')
    credits: Decimal
    total: Decimal
    updated_at: datetime


class BalanceUpdate(BaseModel):
    id: ID = Field(default_factory=ID.generate)
    price: Decimal
    amount: Decimal
    balance: Decimal
    total: Decimal
    reason: str
    chat_id: Optional[ID] = None
    message_id: Optional[ID] = None
    created_at: datetime = Field(default_factory=datetime.utcnow)
    user_id: ID
    is_active: bool = True
    meta: Optional[dict] = None


class BalanceReportType(enum.IntEnum):
    all = 0
    daily = 1
    monthly = 2


class PayoutStatus(enum.IntEnum):
    new = 0
    pending = 1
    declined = 2
    done = 3


class Payout(BaseModel):
    id: ID = Field(default_factory=ID.generate)
    user_id: ID
    transaction_id: ID
    amount: Decimal
    commission: Decimal
    status: PayoutStatus = PayoutStatus.new
    reason: Optional[str]
    method_id: ID
    data: Dict[str, str]
    created_at: datetime = Field(default_factory=datetime.utcnow)


class PayoutMethod(BaseModel):
    id: ID = Field(default_factory=ID.generate)
    title: str
    slug: str
    commission_percent: Decimal
    commission_fixed: Decimal
    period: str
    order: int
    schema_: Dict[str, Any] = Field(alias='schema')


class PaymentProfileBase(BaseModel):
    status: PaymentProfileStatus
    reason: Optional[str]
    first_name: str
    last_name: str
    state: str
    city: str
    address: str
    zip: str
    birthday: datetime
    country: str
    resident: ResidentType
    document_type: DocumentType


class PaymentProfileCreate(PaymentProfileBase):
    id: Optional[ID]
    user_id: Optional[ID]
    updated_at: datetime = Field(default_factory=datetime.utcnow)
    document_front: Optional[ID]
    document_back: Optional[ID]


class PaymentProfile(PaymentProfileBase):
    document_front: Optional[Thumbnail]
    document_back: Optional[Thumbnail]


class PaymentPlan(BaseModel):
    id: str
    price: float
    credits: int


class PaymentIntentRequest(BaseModel):
    plan: str


class CheckPaymentRequest(BaseModel):
    id: str


class DeletePaymentMethodRequest(BaseModel):
    id: str


class PayoutRequest(BaseModel):
    id: ID
    data: Dict[str, str]


class CheckUsernameRequest(Username):
    username: str


class CheckInviteRequest(BaseModel):
    invite: str


class ForgotPasswordRequest(BaseModel):
    email: EmailStr


class CheckResetPasswordRequest(BaseModel):
    token: str


class ResetPasswordRequest(BaseModel):
    token: str
    password: str


class ChangePasswordRequest(BaseModel):
    password_old: str
    password_new: str


class ChangeEmailRequest(BaseModel):
    email: EmailStr
    token: Optional[str] = None


class ChangePhoneRequest(BaseModel):
    phone: str
    code: Optional[str] = None

    @validator('phone')
    def validate_phone(cls, v):
        if type(v) is str:
            v = ''.join(filter(str.isdigit, v))

        return v


class VerifyEmailRequest(BaseModel):
    email: EmailStr
    token: Optional[str] = None


class Event(BaseModel):
    id: str


class EventMessage(Event):
    id = 'message'
    with_id: ID
    client_message_id: ID
    message: Message


class EventBalance(Event):
    id = 'balance'
    user_id: ID
    value: Decimal
    total: Optional[Decimal]
    pending: Optional[Decimal]
    available: Optional[Decimal]
    waiting: Optional[Decimal]
    currency: str


class EventChatCreated(Event):
    id = 'chat_created'
    user_id: ID
    chat_id: ID
    username: str
    name: str


class EventChatAction(Event):
    id = 'chat_action'
    user_id: ID
    action: ChatAction


class EventMessageUpdate(Event):
    id = 'message_update'
    user_id: ID
    messages: List[MessageUpdate]


class EventStatusUpdate(Event):
    id = 'status_update'
    user_id: ID
    status: str
    last_seen: str


class GetProfileMediaRequest(BaseModel):
    user_id: ID


class SetProfileMediaRequest(BaseModel):
    ids: List[ID]


class PayoutRequestFull(BaseModel):
    id: ID
    user_id: ID
    transaction_id: ID
    amount: Decimal
    commission: Decimal
    status: PayoutStatus
    reason: Optional[str]
    method_id: ID
    data: Dict[str, str]
    created_at: datetime
    username: str
    email: str
    first_name: str
    last_name: str
    country: str
    tax_id: Optional[str]
    schema_: Dict[str, Any] = Field(alias='schema')
    title: str

    def format_amount(self) -> str:
        return '$%2.f' % self.amount

    def format_details(self) -> str:
        details = [self.title]

        try:
            data = self.data
            properties = self.schema_['properties']

            for key, value in data.items():
                title = properties[key].get('title', key)
                details.append(f'{title}: {value}')
        except Exception:
            pass

        return ', '.join(details)


class AdminGetPaymentProfilesRequest(BaseModel):
    status: Optional[PaymentProfileStatus] = None


class AdminUpdatePaymentProfileRequest(BaseModel):
    id: ID
    status: PaymentProfileStatus
    reason: Optional[str] = None


class AdminGetPayoutsRequest(BaseModel):
    status: Optional[PayoutStatus] = None


class AdminUpdatePayoutRequest(BaseModel):
    id: ID
    status: PayoutStatus
    reason: Optional[str] = None
