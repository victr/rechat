"""Phone verified user field

Revision ID: 1e3e4b26bde2
Revises: 8cf1a4614f80
Create Date: 2021-04-16 15:58:33.368114

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1e3e4b26bde2'
down_revision = '8cf1a4614f80'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('user', sa.Column('phone_verified', sa.Boolean(), server_default='FALSE', nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('user', 'phone_verified')
    # ### end Alembic commands ###
