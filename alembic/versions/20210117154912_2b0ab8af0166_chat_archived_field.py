"""Chat archived field

Revision ID: 2b0ab8af0166
Revises: e7ddd52475ad
Create Date: 2021-01-17 15:49:12.526495

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2b0ab8af0166'
down_revision = 'e7ddd52475ad'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('chat_users', 'deleted', new_column_name='archived')


def downgrade():
    op.alter_column('chat_users', 'archived', new_column_name='deleted')
