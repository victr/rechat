"""PayoutMethod, Payout, PayoutMethodCountry tables

Revision ID: 2ae4d3861126
Revises: d222161810db
Create Date: 2021-05-27 13:42:32.165051

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '2ae4d3861126'
down_revision = 'd222161810db'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('payout_method',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('title', sa.String(length=100), nullable=False),
    sa.Column('slug', sa.String(length=100), nullable=False),
    sa.Column('commission', sa.String(length=30), nullable=False),
    sa.Column('period', sa.String(length=30), nullable=False),
    sa.Column('order', sa.Integer(), nullable=False),
    sa.Column('schema', sa.String(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('payout',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('transaction_id', postgresql.UUID(), nullable=False),
    sa.Column('status', sa.SmallInteger(), nullable=False),
    sa.Column('reason', sa.String(), nullable=True),
    sa.Column('amount', sa.Numeric(precision=10, scale=2), nullable=True),
    sa.Column('user_id', postgresql.UUID(), nullable=False),
    sa.Column('method_id', postgresql.UUID(), nullable=False),
    sa.ForeignKeyConstraint(['method_id'], ['payout_method.id'], ondelete='no action'),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='no action'),
    sa.PrimaryKeyConstraint('id', 'created_at')
    )
    op.create_index(op.f('ix_payout_created_at'), 'payout', ['created_at'], unique=False)
    op.create_index(op.f('ix_payout_method_id'), 'payout', ['method_id'], unique=False)
    op.create_index(op.f('ix_payout_user_id'), 'payout', ['user_id'], unique=False)
    op.create_table('payout_method_country',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('country', sa.String(length=2), nullable=True),
    sa.Column('method_id', postgresql.UUID(), nullable=False),
    sa.ForeignKeyConstraint(['method_id'], ['payout_method.id'], ondelete='no action'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_payout_method_country_country'), 'payout_method_country', ['country'], unique=False)

    conn = op.get_bind()

    conn.execute("SELECT create_hypertable('payout', 'created_at')")


def downgrade():
    op.drop_index(op.f('ix_payout_method_country_country'), table_name='payout_method_country')
    op.drop_table('payout_method_country')
    op.drop_index(op.f('ix_payout_user_id'), table_name='payout')
    op.drop_index(op.f('ix_payout_method_id'), table_name='payout')
    op.drop_index(op.f('ix_payout_created_at'), table_name='payout')
    op.drop_table('payout')
    op.drop_table('payout_method')
