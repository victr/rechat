"""Chat deleted field

Revision ID: e7ddd52475ad
Revises: 0ccb88cc07aa
Create Date: 2021-01-15 18:37:49.109244

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e7ddd52475ad'
down_revision = '0ccb88cc07aa'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('chat_users', sa.Column('deleted', sa.Boolean(), server_default='FALSE', nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('chat_users', 'deleted')
    # ### end Alembic commands ###
