"""rename file path field to url

Revision ID: ab97187203ad
Revises: 844ee0dce82a
Create Date: 2021-03-13 15:20:01.317437

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ab97187203ad'
down_revision = '844ee0dce82a'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('file', 'path', new_column_name='url')


def downgrade():
    op.alter_column('file', 'url', new_column_name='path')
