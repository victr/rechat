"""Payment profile address

Revision ID: d222161810db
Revises: 2f66f9d9ea98
Create Date: 2021-05-24 21:59:43.962520

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd222161810db'
down_revision = '2f66f9d9ea98'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('payment_profile', sa.Column('address', sa.String(), nullable=True))
    op.add_column('payment_profile', sa.Column('city', sa.String(), nullable=True))
    op.add_column('payment_profile', sa.Column('last_name', sa.String(), nullable=True))
    op.add_column('payment_profile', sa.Column('state', sa.String(), nullable=True))
    op.add_column('payment_profile', sa.Column('zip', sa.String(), nullable=True))
    op.alter_column('payment_profile', 'name', new_column_name='first_name')


def downgrade():
    op.alter_column('payment_profile', 'first_name', new_column_name='name')
    op.drop_column('payment_profile', 'zip')
    op.drop_column('payment_profile', 'state')
    op.drop_column('payment_profile', 'last_name')
    op.drop_column('payment_profile', 'city')
    op.drop_column('payment_profile', 'address')
