"""BalanceChange price field

Revision ID: 99616802eacb
Revises: 1feadbcf6217
Create Date: 2021-03-25 21:03:02.110855

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '99616802eacb'
down_revision = '1feadbcf6217'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('balance_change', 'amount', new_column_name='price')


def downgrade():
    op.alter_column('balance_change', 'price', new_column_name='amount')
