"""Payout commission field

Revision ID: 234fea5234ff
Revises: 4d63b28d2125
Create Date: 2021-05-29 17:21:15.067902

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '234fea5234ff'
down_revision = '4d63b28d2125'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('payout', sa.Column('commission', sa.Numeric(precision=10, scale=2), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('payout', 'commission')
    # ### end Alembic commands ###
