"""BalanceChange credits field renamed to amount

Revision ID: ba586d813219
Revises: 99616802eacb
Create Date: 2021-03-25 21:05:32.476060

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ba586d813219'
down_revision = '99616802eacb'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('balance_change', 'credits', new_column_name='amount')


def downgrade():
    op.alter_column('balance_change', 'amount', new_column_name='credits')
