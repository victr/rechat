"""Payout data field

Revision ID: 43afa75635ee
Revises: 234fea5234ff
Create Date: 2021-05-29 17:41:18.746101

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '43afa75635ee'
down_revision = '234fea5234ff'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('payout', sa.Column('data', postgresql.JSONB(astext_type=sa.Text()), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('payout', 'data')
    # ### end Alembic commands ###
