"""Initial

Revision ID: 8732f8028b6f
Revises: 
Create Date: 2020-11-22 22:39:28.706733

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '8732f8028b6f'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('chat',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('email', sa.String(), nullable=False),
    sa.Column('username', sa.String(length=64), nullable=True),
    sa.Column('name', sa.String(length=128), nullable=True),
    sa.Column('hashed_password', sa.String(), nullable=True),
    sa.Column('is_active', sa.Boolean(), nullable=False),
    sa.Column('is_superuser', sa.Boolean(), nullable=False),
    sa.Column('account_type', sa.SmallInteger(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('email_notification', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_user_email'), 'user', ['email'], unique=True)
    op.create_index(op.f('ix_user_username'), 'user', ['username'], unique=True)
    op.create_table('balance',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('credits', sa.Numeric(precision=10, scale=0), nullable=False),
    sa.Column('value', sa.Numeric(precision=10, scale=2), nullable=False),
    sa.Column('updated_at', sa.DateTime(), nullable=False),
    sa.Column('user_id', postgresql.UUID(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='no action'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('balance_change',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('reason', sa.String(length=64), nullable=True),
    sa.Column('credits', sa.Numeric(precision=10, scale=0), nullable=True),
    sa.Column('amount', sa.Numeric(precision=10, scale=2), nullable=True),
    sa.Column('balance', sa.Numeric(precision=10, scale=2), nullable=True),
    sa.Column('message_id', postgresql.UUID(), nullable=True),
    sa.Column('is_active', sa.Boolean(), nullable=False),
    sa.Column('user_id', postgresql.UUID(), nullable=False),
    sa.Column('chat_id', postgresql.UUID(), nullable=True),
    sa.CheckConstraint('balance >= 0'),
    sa.ForeignKeyConstraint(['chat_id'], ['chat.id'], ondelete='no action'),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='no action'),
    sa.PrimaryKeyConstraint('id', 'created_at')
    )
    op.create_index(op.f('ix_balance_change_chat_id'), 'balance_change', ['chat_id'], unique=False)
    op.create_index(op.f('ix_balance_change_created_at'), 'balance_change', ['created_at'], unique=False)
    op.create_index(op.f('ix_balance_change_is_active'), 'balance_change', ['is_active'], unique=False)
    op.create_index(op.f('ix_balance_change_user_id'), 'balance_change', ['user_id'], unique=False)
    op.create_table('chat_users',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('chat_id', postgresql.UUID(), nullable=False),
    sa.Column('user_id', postgresql.UUID(), nullable=False),
    sa.Column('with_id', postgresql.UUID(), nullable=False),
    sa.ForeignKeyConstraint(['chat_id'], ['chat.id'], ondelete='no action'),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='no action'),
    sa.ForeignKeyConstraint(['with_id'], ['user.id'], ondelete='no action'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index('chat_users_user_id_idx', 'chat_users', ['user_id'], unique=False)
    op.create_table('file',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('uploaded_at', sa.DateTime(), nullable=False),
    sa.Column('size', sa.BigInteger(), nullable=False),
    sa.Column('mime', sa.String(length=64), nullable=True),
    sa.Column('path', sa.String(length=2048), nullable=False),
    sa.Column('meta', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
    sa.Column('uploaded_by', postgresql.UUID(), nullable=False),
    sa.ForeignKeyConstraint(['uploaded_by'], ['user.id'], ondelete='no action'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('oauth_account',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('name', sa.String(length=32), nullable=False),
    sa.Column('account_id', sa.String(), nullable=False),
    sa.Column('account_email', sa.String(), nullable=False),
    sa.Column('access_token', sa.String(), nullable=False),
    sa.Column('expires_at', sa.DateTime(), nullable=False),
    sa.Column('refresh_token', sa.String(), nullable=True),
    sa.Column('user_id', postgresql.UUID(), nullable=False),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='cascade'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_oauth_account_account_id'), 'oauth_account', ['account_id'], unique=False)
    op.create_index(op.f('ix_oauth_account_name'), 'oauth_account', ['name'], unique=False)
    op.create_table('message',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('created_at', sa.DateTime(), nullable=False),
    sa.Column('type', sa.Integer(), nullable=False),
    sa.Column('text', sa.String(length=500), nullable=True),
    sa.Column('is_deleted', sa.Boolean(), nullable=False),
    sa.Column('data', postgresql.JSONB(astext_type=sa.Text()), nullable=True),
    sa.Column('sender_id', postgresql.UUID(), nullable=False),
    sa.Column('chat_id', postgresql.UUID(), nullable=False),
    sa.Column('file_id', postgresql.UUID(), nullable=True),
    sa.ForeignKeyConstraint(['chat_id'], ['chat.id'], ondelete='no action'),
    sa.ForeignKeyConstraint(['file_id'], ['file.id'], ondelete='no action'),
    sa.ForeignKeyConstraint(['sender_id'], ['user.id'], ondelete='no action'),
    sa.PrimaryKeyConstraint('id', 'created_at')
    )
    op.create_index(op.f('ix_message_created_at'), 'message', ['created_at'], unique=False)
    op.create_index(op.f('ix_message_type'), 'message', ['type'], unique=False)
    op.create_table('profile_celebrity',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('bio', sa.String(length=500), nullable=True),
    sa.Column('website', sa.String(length=2084), nullable=True),
    sa.Column('facebook', sa.String(length=100), nullable=True),
    sa.Column('instagram', sa.String(length=100), nullable=True),
    sa.Column('twitter', sa.String(length=100), nullable=True),
    sa.Column('snapchat', sa.String(length=100), nullable=True),
    sa.Column('youtube', sa.String(length=100), nullable=True),
    sa.Column('price', sa.Integer(), nullable=False),
    sa.Column('category', sa.Integer(), nullable=True),
    sa.Column('phone', sa.String(length=20), nullable=True),
    sa.Column('latitude', sa.Float(), nullable=True),
    sa.Column('longitude', sa.Float(), nullable=True),
    sa.Column('place_id', sa.String(length=100), nullable=True),
    sa.Column('location', sa.String(length=200), nullable=True),
    sa.Column('greeting', sa.String(length=100), nullable=True),
    sa.Column('user_id', postgresql.UUID(), nullable=False),
    sa.Column('photo', postgresql.UUID(), nullable=True),
    sa.ForeignKeyConstraint(['photo'], ['file.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='cascade'),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('profile_fan',
    sa.Column('id', postgresql.UUID(), nullable=False),
    sa.Column('bio', sa.String(length=500), nullable=True),
    sa.Column('user_id', postgresql.UUID(), nullable=False),
    sa.Column('photo', postgresql.UUID(), nullable=True),
    sa.ForeignKeyConstraint(['photo'], ['file.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ondelete='cascade'),
    sa.PrimaryKeyConstraint('id')
    )

    conn = op.get_bind()

    conn.execute("SELECT create_hypertable('balance_change', 'created_at')")
    conn.execute("SELECT create_hypertable('message', 'created_at')")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('profile_fan')
    op.drop_table('profile_celebrity')
    op.drop_index(op.f('ix_message_type'), table_name='message')
    op.drop_index(op.f('ix_message_created_at'), table_name='message')
    op.drop_table('message')
    op.drop_index(op.f('ix_oauth_account_name'), table_name='oauth_account')
    op.drop_index(op.f('ix_oauth_account_account_id'), table_name='oauth_account')
    op.drop_table('oauth_account')
    op.drop_table('file')
    op.drop_index('chat_users_user_id_idx', table_name='chat_users')
    op.drop_table('chat_users')
    op.drop_index(op.f('ix_balance_change_user_id'), table_name='balance_change')
    op.drop_index(op.f('ix_balance_change_is_active'), table_name='balance_change')
    op.drop_index(op.f('ix_balance_change_created_at'), table_name='balance_change')
    op.drop_index(op.f('ix_balance_change_chat_id'), table_name='balance_change')
    op.drop_table('balance_change')
    op.drop_table('balance')
    op.drop_index(op.f('ix_user_username'), table_name='user')
    op.drop_index(op.f('ix_user_email'), table_name='user')
    op.drop_table('user')
    op.drop_table('chat')
    # ### end Alembic commands ###
