import enum


class Error(enum.Enum):

    UserExists = 'User already exists'
    UsernameExists = 'Username already exists'
    ProfileNotFound = 'Profile not found'


class DatabaseError(Exception):
    pass


class DoesNotExist(DatabaseError):
    pass


class InsufficientFunds(DatabaseError):
    pass
