import os
import logging
import mimetypes

import arq
import sentry_sdk
from fastapi import FastAPI, Depends, Request
from fastapi.responses import Response, FileResponse, JSONResponse
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from starlette.datastructures import Headers
from starlette.types import Scope
from starlette.staticfiles import NotModifiedResponse

import models
import auth
import user
import chat
import billing
import config
import files
import admin


logging.basicConfig()
logging.getLogger('databases').setLevel(logging.DEBUG)
logging.getLogger('chat').setLevel(logging.DEBUG)


async def startup():
    redis = await arq.create_pool(arq.connections.RedisSettings(host=config.REDIS_HOST))
    chat.manager.start(redis)
    app.redis = redis

    if config.SENTRY_DSN:
        sentry_sdk.init(config.SENTRY_DSN, traces_sample_rate=1.0)


app = FastAPI(
    on_startup=[models.startup, startup],
    on_shutdown=[models.shutdown],
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=('*', ),
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

app.include_router(auth.router, prefix='/api/auth', tags=['auth'])
app.include_router(user.router, prefix='/api/user', tags=['user'])
app.include_router(chat.router, prefix='/api/chat', tags=['chat'])
app.include_router(billing.router, prefix='/api/billing', tags=['billing'])
app.include_router(files.router, prefix='/api/storage', tags=['storage'])
app.include_router(admin.router, prefix='/api/admin', tags=['admin'])


@app.exception_handler(Exception)
async def exception_handler(request: Request, exc: Exception):
    if config.SENTRY_DSN:
        sentry_sdk.capture_exception(exc)

    return JSONResponse(
        status_code=418,
        content={'detail': 'Oops! Something has gone wrong'},
    )


if __name__ == '__main__':
    import uvicorn

    uvicorn.run(
        app,
        host='0.0.0.0',
        port=8000,
        loop='uvloop',
        http='httptools',
        ws='websockets',
        lifespan='on',
    )
