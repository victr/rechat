import random
import asyncio
from datetime import timedelta
from typing import Union

from fastapi import APIRouter, Depends, HTTPException, status, Security
from fastapi.responses import JSONResponse, HTMLResponse, Response, RedirectResponse, FileResponse
from fastapi.requests import Request
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.pwd import genword
from passlib.context import CryptContext
from twilio.rest import Client as TwilioClient

import models
import crud
from config import *
from schemas import *
from errors import Error
import services.sms
import services.mail
from oauth import GoogleOAuth2Client, FacebookOAuth2Client, TwitterOAuth2Client, OAuth2ClientError
from depends import RedisConnection, get_request_meta_data
import schemas


STATE_TOKEN_AUDIENCE = 'oauth'
JWT_ALGORITHM = jwt.ALGORITHMS.HS256

router = APIRouter()


oauth_clients = {
    'google': GoogleOAuth2Client(
        config.get('GOOGLE_CLIENT_ID'),
        config.get('GOOGLE_CLIENT_SECRET'),
    ),
    'facebook': FacebookOAuth2Client(
        config.get('FACEBOOK_CLIENT_ID'),
        config.get('FACEBOOK_CLIENT_SECRET'),
    ),
    'twitter': TwitterOAuth2Client(
        config.get('TWITTER_CLIENT_ID'),
        config.get('TWITTER_CLIENT_SECRET'),
    ),
}


pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')


oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl='/api/auth/jwt/login',
)


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


async def authenticate_user(form: UserLogin) -> Optional[models.User]:
    user = await crud.get_user_by_email(form.email)

    if user and verify_password(form.password, user.hashed_password):
        return user


def encode_token(data: dict, expires_delta: Optional[timedelta] = None) -> str:
    to_encode = data.copy()

    if expires_delta is None:
        expires_delta = timedelta(minutes=15)

    expire = datetime.utcnow() + expires_delta

    to_encode.update({'exp': expire})

    return jwt.encode(to_encode, SECRET_KEY, algorithm=JWT_ALGORITHM)


def create_token(user: Union[UserCreate, models.User], expire_minutes: int) -> schemas.Token:
    access_token_expires = timedelta(minutes=expire_minutes)

    token_data = TokenData(
        id=user.id,
        account_type=user.account_type,
    )

    data = token_data.dict()
    data['id'] = str(data['id'])

    access_token = encode_token(
        data=data,
        expires_delta=access_token_expires,
    )

    return schemas.Token(access_token=access_token)


def decode_token(token: str):
    return jwt.decode(
        token,
        SECRET_KEY,
        audience=STATE_TOKEN_AUDIENCE,
        algorithms=[JWT_ALGORITHM],
    )


def get_token_data(token: str = Depends(oauth2_scheme)) -> TokenData:
    try:
        payload = decode_token(token)
        token_data = TokenData(**payload)
    except (JWTError, Exception):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Could not validate credentials',
            headers={'WWW-Authenticate': 'Bearer'},
        )

    return token_data


async def get_current_user(token_data: TokenData = Depends(get_token_data)) -> models.User:
    return await crud.get_user_by_id(token_data.id)


@router.post('/jwt/login', response_model=Token)
async def login(form: OAuth2PasswordRequestForm = Depends(), meta=Depends(get_request_meta_data)):
    user = await authenticate_user(UserLogin(email=form.username, password=form.password))

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Bearer'},
        )

    await crud.update_user(user.id, {
        'last_login_at': datetime.now(),
        **meta,
    })

    return create_token(user, ACCESS_TOKEN_EXPIRE_MINUTES)


@router.post('/register', response_model=Token)
async def register(form: UserRegister, redis: RedisConnection = Depends(), meta=Depends(get_request_meta_data)):
    async with crud.database.transaction():
        invite = await crud.get_invite(form.invite)

        if form.account_type.is_celebrity() and not invite:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Invalid invite code')

        email = form.email
        user = await crud.get_user_by_email(email)

        if user is not None:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, Error.UserExists.value)

        user_create = UserCreate(
            email=email,
            hashed_password=get_password_hash(form.password),
            is_active=True,
            is_superuser=False,
            account_type=form.account_type,
            email_verified=False,
            email_notification=True,
            invite=invite,
            last_login_at=datetime.now(),
            **meta,
        )

        if form.account_type.is_fan():
            user_create.username = genword(length=8)

        user = await crud.create_user(user_create)

        if form.account_type.is_celebrity():
            await redis.task('email_celebrity_registered', user.id, email)
            await redis.task('email_celebrity_share_link', user.id, _defer_by=timedelta(days=2))
        elif form.account_type.is_fan():
            await redis.task('email_fan_registered', user.id, email)

        await redis.task('notify_new_user', user.id, email, form.account_type)

        return create_token(user, ACCESS_TOKEN_EXPIRE_MINUTES)


@router.post('/sendOtpCode')
async def login_otp(request: OtpCodeRequest, redis: RedisConnection = Depends()):
    client = TwilioClient(TWILIO_SID, TWILIO_TOKEN)

    code = '%04d' % random.randint(0, 9999)

    await redis.otp_create_code(request.phone, code)

    from_number = TWILIO_PHONE_NUMBER if request.phone.startswith('1') else 'Rechat'

    client.messages.create(
        body=f'Rechat code: {code}',
        from_=from_number,
        to=request.phone,
    )

    return True


@router.post('/confirmOtpCode', response_model=LoginOtpResponse)
async def login_otp_confirm(request: OtpCodeRequest, redis: RedisConnection = Depends()):
    result = await redis.otp_get_code(request.phone)

    if result is None:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Code is expired')

    if request.code != result:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Wrong code')

    new_user = False
    user = await crud.get_user_by_phone(request.phone)

    if not user:
        invite = await crud.get_invite(request.invite)

        if not invite:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Invalid invite')

        if not request.account_type:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Invalid account type')

        new_user = True
        user = await crud.create_user(UserCreate(
            phone=request.phone,
            hashed_password=None,
            is_active=True,
            is_superuser=False,
            account_type=request.account_type,
            phone_verified=True,
            email_verified=False,
            email_notification=True,
            invite=invite,
        ))
    else:
        await crud.update_user(user.id, {
            'phone_verified': True,
        })

    token = create_token(user, ACCESS_TOKEN_EXPIRE_MINUTES)

    return {
        'new_user': new_user,
        'token': token,
    }


@router.get('/oauth/welcome')
async def oauth_welcome():
    return FileResponse('static/templates/oauth/welcome.html')


@router.get('/oauth/error')
async def oauth_error():
    return FileResponse('static/templates/oauth/error.html')


@router.get('/oauth/{client_name}/{action}/{account_type}', response_model=AuthorizationUrl)
async def oauth_client(client_name: str, action: str, account_type: AccountType, request: Request):
    client = oauth_clients.get(client_name)

    actions = ('login', 'register')

    if not client or action not in actions:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    redirect_uri = request.url_for('oauth_callback', client_name=client_name)

    url = await client.get_authorization_url(redirect_uri, account_type=account_type, action=action)

    return RedirectResponse(url)


@router.get('/oauth/{client_name}/callback')
async def oauth_callback(request: Request, client_name: str, redis: RedisConnection = Depends(), meta=Depends(get_request_meta_data)):
    client = oauth_clients.get(client_name)

    params = dict(request.query_params.items())

    if not client:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    try:
        response = await client.get_token_data(**params)
        action = response.state['action']
        account_type = AccountType(response.state['account_type'])
    except OAuth2ClientError:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, 'invalid_state')

    account = OAuthAccount(
        name=response.name,
        access_token=response.access_token,
        account_id=response.account_id,
        account_email=response.account_email,
        expires_at=response.expires_at,
    )

    if action == 'register':
        user = UserCreate(
            email=account.account_email,
            is_active=True,
            is_superuser=False,
            account_type=account_type,
            last_login_at=datetime.now(),
            **meta,
        )

        if account_type.is_fan():
            user.username = genword(length=8)

        async with crud.database.transaction():
            existed_user = await crud.get_user_by_email(account.account_email)

            if existed_user:
                user.id = existed_user.id
            else:
                await crud.create_user(user)

            await crud.connect_oauth_account(user.id, account_type, account, response.name)

        if account_type.is_celebrity():
            await redis.task('email_celebrity_registered', user.id, user.email)
            await redis.task('email_celebrity_share_link', user.id, _defer_by=timedelta(days=2))
        elif account_type.is_fan():
            await redis.task('email_fan_registered', user.id, user.email)

        await redis.task('notify_new_user', user.id, user.email, account_type)

    else:
        oauth_account = await crud.get_user_oauth_account_by_email(account.account_email, response.name)

        async with crud.database.transaction():
            if oauth_account:
                user = await crud.get_user_by_id(oauth_account['user_id'])
            else:
                user = await crud.get_user_by_email(account.account_email)

            if not user:
                redirect_url = request.url_for('oauth_error').replace('http:', 'https:')
                return RedirectResponse(redirect_url)

            if user.account_type == AccountType.celebrity:
                await crud.connect_oauth_account(user.id, account_type, account, response.name)

            await crud.update_user(user.id, {
                'last_login_at': datetime.now(),
                **meta,
            })

    token = create_token(user, ACCESS_TOKEN_EXPIRE_MINUTES)

    redirect_url = request.url_for('oauth_welcome').replace('http:', 'https:')

    response = RedirectResponse(redirect_url)
    response.set_cookie('token_type', token.token_type, max_age=15)
    response.set_cookie('access_token', token.access_token, max_age=15)

    return response


@router.post('/oauth/{client_name}/connect')
async def oauth_bind_account(request: Request, client_name: str, token_data: TokenData = Depends(get_token_data)):
    client = oauth_clients.get(client_name)

    if not client:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    redirect_uri = request.url_for('oauth_bind_account_callback', client_name=client_name)

    url = await client.get_authorization_url(redirect_uri, user_id=str(token_data.id), account_type=token_data.account_type)

    return {'authorization_url': str(url)}


@router.get('/oauth/{client_name}/connect')
async def oauth_bind_account_callback(request: Request, client_name: str):
    client = oauth_clients.get(client_name)

    params = dict(request.query_params.items())

    if not client:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    try:
        response = await client.get_token_data(**params)
        user_id = response.state['user_id']
        account_type = schemas.AccountType(response.state['account_type'])
    except OAuth2ClientError:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, 'invalid_state')

    account = OAuthAccount(
        name=response.name,
        user_id=user_id,
        username=response.username,
        access_token=response.access_token,
        account_id=response.account_id,
        account_email=response.account_email,
        expires_at=response.expires_at,
    )

    await crud.connect_oauth_account(user_id, account_type, account, client_name)

    return RedirectResponse(f'/profile/{client_name}')


@router.post('/oauth/{client_name}/disconnect')
async def oauth_disconnect_account(client_name: str, token_data: TokenData = Depends(get_token_data)):
    account_id = await crud.disconnect_oauth_account(token_data.id, token_data.account_type, client_name)

    return account_id is not None


@router.post('/verify/email')
async def send_verification_link(request: VerifyEmailRequest, token_data: TokenData = Depends(get_token_data), redis: RedisConnection = Depends()):

    if request.token is None:
        user = await crud.get_user_by_id(token_data.id)

        if user.email_verified:
            raise HTTPException(status.HTTP_403_FORBIDDEN, 'Email already has been verified')

        token = encode_token({
            'id': str(token_data.id),
            'email': request.email,
        }, expires_delta=timedelta(days=7))

        await redis.task(
            'email_verify',
            user_id=token_data.id,
            email=request.email,
            name=user.name or user.username,
            token=token,
        )

        return request.email


@router.get('/verify/email/{token}')
async def confirm_email(token: str, redis: RedisConnection = Depends()):
    try:
        data = decode_token(token)
    except JWTError:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Invalid token')

    user_id = data.get('id')
    email = data.get('email')

    if not email:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Invalid email')

    redirect_url = '/profile/email'

    async with crud.database.transaction():
        user = await crud.get_user_by_id(user_id)

        if AccountType(user.account_type).is_fan() and not user.email_verified:
            balance, _ = await crud.balance_update(
                user_id=user_id,
                amount=PROMO_FAN_BONUS_EMAIL_CONFIRM,
                reason='bonus',
                is_active=True,
            )

            await redis.publish_event(EventBalance(
                user_id=user_id,
                value=balance.credits,
                total=balance.credits,
                currency='credits',
            ))

            redirect_url += '/verified'

        await crud.update_user(user_id, {
            'email': email,
            'email_verified': True,
        })

    redirect = create_autologin_url(user_id, redirect_url)

    return RedirectResponse(redirect)


@router.post('/invite/check')
async def check_invite(request: CheckInviteRequest):
    invite = await crud.get_invite(request.invite)

    return bool(invite)


@router.post('/password/forgot')
async def forgot_password(request: ForgotPasswordRequest, req: Request):
    user = await crud.get_user_by_email(request.email)

    if not user:
        await asyncio.sleep(0.5)
        return

    token = encode_token({'id': str(user.id)}, expires_delta=timedelta(hours=24))

    await services.mail.password_reset(user.email, token, req.headers.get('host'))


@router.post('/password/check')
async def check_reset_password_token(request: CheckResetPasswordRequest):
    try:
        data = decode_token(request.token)
    except JWTError:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Invalid token')

    user_id = data.get('id')

    profile = await crud.get_base_profile(user_id)

    if not profile:
        raise HTTPException(status.HTTP_404_NOT_FOUND, 'User not found')

    return profile


@router.post('/password/reset')
async def reset_password(request: ResetPasswordRequest, redis: RedisConnection = Depends()):
    data = decode_token(request.token)

    user_id = data.get('id')

    user = await crud.get_user_by_id(user_id)

    if not user:
        raise HTTPException(status.HTTP_404_NOT_FOUND)

    hashed_password = get_password_hash(request.password)

    await crud.update_user(user_id, {
        'email_verified': True,
        'hashed_password': hashed_password,
    })

    token = create_token(user, ACCESS_TOKEN_EXPIRE_MINUTES)

    await redis.task('email_password_changed', user_id, user.email)

    return token


@router.post('/password/change')
async def change_password(request: ChangePasswordRequest, token: TokenData = Depends(get_token_data), redis: RedisConnection = Depends()):

    async with models.database.transaction():
        user = await crud.get_user_by_id(token.id)

        if not verify_password(request.password_old, user.hashed_password):
            raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Old password is wrong')

        hashed_password = get_password_hash(request.password_new)

        await crud.update_user(user.id, {
            'hashed_password': hashed_password,
        })

        await redis.task('email_password_changed', user.id, user.email)

    return True


@router.post('/email/change')
async def change_email(request: ChangeEmailRequest, token: TokenData = Depends(get_token_data), redis: RedisConnection = Depends()):
    user = await crud.get_user_by_email(request.email)

    if user and user.id != token.id and user.email_verified:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, 'This Email is already connected to another user')

    token = encode_token({'id': str(token.id)}, expires_delta=timedelta(hours=24))


@router.post('/phone/change')
async def change_phone(request: ChangePhoneRequest, token: TokenData = Depends(get_token_data), redis: RedisConnection = Depends()):
    user = await crud.get_user_by_phone(request.phone)

    if user and user.id != token.id:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, 'This phone is already connected to another user')

    if user and user.phone_verified:
        raise HTTPException(status.HTTP_400_BAD_REQUEST, 'This phone number is already verified')

    if not request.code:
        code = '%04d' % random.randint(0, 9999)

        await redis.otp_create_code(request.phone, code)

        await services.sms.send_message(
            request.phone,
            f'Rechat phone confirmation code: {code}'
        )
    else:
        result = await redis.otp_get_code(request.phone)

        if result is None:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Code is expired')

        if request.code != result:
            raise HTTPException(status.HTTP_400_BAD_REQUEST, 'Wrong code')

        await crud.update_user(token.id, {
            'phone': request.phone,
            'phone_verified': True,
        })

    return True


@router.get('/login/{token}')
async def autologin(token: str, meta=Depends(get_request_meta_data)):
    try:
        data = decode_token(token)
        a = AutologinToken(**data)

        user = await crud.get_user_by_id(a.user_id)

        await crud.update_user(user.id, {
            'last_login_at': datetime.now(),
            **meta,
        })

        t = create_token(user, ACCESS_TOKEN_EXPIRE_MINUTES)

        response = RedirectResponse(a.redirect_url)
        response.set_cookie('token_type', t.token_type, max_age=10)
        response.set_cookie('access_token', t.access_token, max_age=10)

        return response

    except (JWTError, Exception):
        pass

    return RedirectResponse('/')


def create_autologin_url(user_id: schemas.ID, redirect_url: str):
    token = encode_token({'id': str(user_id), 'u': redirect_url}, expires_delta=timedelta(minutes=AUTOLOGIN_TOKEN_EXPIRE_MINUTES))

    return f'{BASE_URL}/api/auth/login/{token}'


class Token:
    def __init__(self, token: str = Depends(oauth2_scheme)):
        data = get_token_data(token)

        self.id = data.id
        self.account_type = data.account_type
