import ujson
import databases
from sqlalchemy import (
    ForeignKey,
    Column,
    String,
    Boolean,
    DateTime,
    SmallInteger,
    BigInteger,
    Integer,
    Float,
    Numeric,
    Index,
    CheckConstraint,
    TypeDecorator,
    PrimaryKeyConstraint,
)
from asyncpg.exceptions import UniqueViolationError
from sqlalchemy.orm import deferred, column_property
from sqlalchemy.dialects.postgresql import UUID, JSONB, INET
from sqlalchemy.ext.declarative import declarative_base, declared_attr, DeclarativeMeta

from config import *


database = databases.Database(
    DATABASE_URL,
    min_size=5,
    max_size=20,
)


class BaseMixin:

    @classmethod
    def select(cls, *args):
        return cls.__table__.select(*args)

    @classmethod
    def insert(cls):
        return cls.__table__.insert()

    @classmethod
    def update(cls):
        return cls.__table__.update()

    @classmethod
    def delete(cls):
        return cls.__table__.delete()

    @classmethod
    def outerjoin(cls, *args, **kwargs):
        return cls.__table__.outerjoin(*args, **kwargs)

    @classmethod
    def join(cls, table, *args, **kwargs):
        return cls.__table__.join(table, *args, *kwargs)


class JSON(TypeDecorator):
    impl = JSONB

    def process_bind_param(self, value, dialect):
        return value

    def process_result_value(self, value, dialect):
        if value:
            return ujson.loads(value)


Base: DeclarativeMeta = declarative_base(cls=BaseMixin)


async def startup():
    await database.connect()


async def shutdown():
    await database.disconnect()


class User(Base):
    __tablename__ = 'user'

    id = Column(UUID, primary_key=True)
    email = Column(String, unique=True, index=True, nullable=True)
    phone = Column(String(20), unique=True, index=True, nullable=True)
    username = Column(String(64), unique=True, index=True, nullable=True)
    name = Column(String(128))
    hashed_password = Column(String, nullable=True)
    is_active = Column(Boolean, default=True, nullable=False)
    is_superuser = Column(Boolean, default=False, nullable=False)
    account_type = Column(SmallInteger, default=False, nullable=False)
    gender = Column(SmallInteger, nullable=True)
    birthday = Column(DateTime, nullable=True)
    created_at = Column(DateTime, nullable=False)
    phone_verified = Column(Boolean, nullable=False, server_default='FALSE')
    email_verified = Column(Boolean, nullable=False, server_default='FALSE')
    email_notification = Column(Boolean, nullable=True)
    ip_address = deferred(Column(INET, nullable=True))
    ip_country = deferred(Column(String(2), nullable=True))
    last_login_at = deferred(Column(DateTime, nullable=True))

    @declared_attr
    def photo(cls):
        return Column(UUID, ForeignKey('file.id'))

    @declared_attr
    def invite(cls):
        return Column(UUID, ForeignKey('invite.id'), nullable=True)


class OAuthAccount(Base):
    __tablename__ = 'oauth_account'

    id = Column(UUID, primary_key=True)
    name = Column(String(32), index=True, nullable=False)
    account_id = Column(String, index=True, nullable=False)
    account_email = Column(String, nullable=False)
    username = Column(String, nullable=True)
    access_token = Column(String, nullable=False)
    expires_at = Column(DateTime, nullable=False)
    refresh_token = Column(String, nullable=True)

    @declared_attr
    def user_id(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='cascade'), nullable=False)


class File(Base):
    __tablename__ = 'file'

    id = Column(UUID, primary_key=True)
    uploaded_at = Column(DateTime, nullable=False)
    size = Column(BigInteger, nullable=False)
    mime = Column(String(64), nullable=True)
    url = Column(String(2048), nullable=False)
    meta = Column(JSONB, nullable=True)

    @declared_attr
    def uploaded_by(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=False)


class ProfileBase:
    id = Column(UUID, primary_key=True)

    @declared_attr
    def user_id(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='cascade'), nullable=False)

    bio = Column(String(500))


class ProfileSocial:
    website = Column(String(2084))
    facebook = Column(String(100))
    instagram = Column(String(100))
    twitter = Column(String(100))
    snapchat = Column(String(100))
    google = Column(String(100))


class CelebrityProfile(ProfileBase, ProfileSocial, Base):
    __tablename__ = 'profile_celebrity'

    price = Column(Numeric(precision=5, scale=2), nullable=False, server_default='0.1')
    category = Column(Integer, nullable=True)
    latitude = Column(Float, nullable=True)
    longitude = Column(Float, nullable=True)
    place_id = Column(String(100))
    location = Column(String(200))
    greeting = Column(String(100))
    onboarding = Column(SmallInteger, nullable=True)
    free_messages = Column(SmallInteger, nullable=False, server_default='1')
    free_messages_enabled = Column(Boolean, nullable=False, server_default='FALSE')

    @declared_attr
    def greeting_video(cls):
        return Column(UUID, ForeignKey('file.id', ondelete='no action'), nullable=True)


class FanProfile(ProfileBase, ProfileSocial, Base):
    __tablename__ = 'profile_fan'


class Chat(Base):
    __tablename__ = 'chat'
    id = Column(UUID, primary_key=True)
    created_at = Column(DateTime)


class ChatUsers(Base):
    __tablename__ = 'chat_users'
    __table_args__ = (Index('chat_users_user_id_idx', 'user_id'), )

    id = Column(UUID, primary_key=True)

    @declared_attr
    def chat_id(cls):
        return Column(UUID, ForeignKey('chat.id', ondelete='no action'), nullable=False)

    @declared_attr
    def user_id(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=False)

    @declared_attr
    def with_id(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=False)

    archived = Column(Boolean, nullable=False, server_default='FALSE')


class Message(Base):
    __tablename__ = 'message'
    __table_args__ = (
        PrimaryKeyConstraint('id', 'created_at'),
    )

    id = Column(UUID)
    created_at = Column(DateTime, nullable=False, index=True)
    type = Column(Integer, nullable=False, index=True)

    @declared_attr
    def sender_id(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=False)

    @declared_attr
    def chat_id(cls):
        return Column(UUID, ForeignKey('chat.id', ondelete='no action'), nullable=False)

    @declared_attr
    def file_id(cls):
        return Column(UUID, ForeignKey('file.id', ondelete='no action'), nullable=True)

    text = Column(String(MESSAGE_MAX_TEXT_LENGTH), nullable=True)
    is_deleted = deferred(Column(Boolean, nullable=False))
    is_read = Column(Boolean, default=False)
    data = Column(JSONB, nullable=True)


class Balance(Base):
    __tablename__ = 'balance'

    id = Column(UUID, primary_key=True)

    @declared_attr
    def user_id(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=False)

    credits = Column(Numeric(precision=10, scale=0), nullable=False)
    value = Column(Numeric(precision=10, scale=2), nullable=False)
    total = Column(Numeric(precision=10, scale=2), nullable=False, server_default='0')
    updated_at = Column(DateTime, nullable=False)


class BalanceChange(Base):
    __tablename__ = 'balance_change'

    __table_args__ = (
        PrimaryKeyConstraint('id', 'created_at'),
        CheckConstraint('balance >= 0'),
    )

    id = Column(UUID)
    created_at = Column(DateTime, nullable=False, index=True)

    @declared_attr
    def user_id(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=False, index=True)

    @declared_attr
    def chat_id(cls):
        return Column(UUID, ForeignKey('chat.id', ondelete='no action'), nullable=True, index=True)

    reason = Column(String(64))
    amount = Column(Numeric(precision=10, scale=0))
    price = Column(Numeric(precision=10, scale=2))
    balance = Column(Numeric(precision=10, scale=2))
    total = Column(Numeric(precision=10, scale=2), server_default='0')
    message_id = Column(UUID, nullable=True)
    is_active = Column(Boolean, nullable=False, index=True)
    meta = Column(JSONB, nullable=True)
    amount_real = column_property(amount * price)


class PayoutMethod(Base):
    __tablename__ = 'payout_method'

    id = Column(UUID, primary_key=True)
    title = Column(String(100), nullable=False)
    slug = Column(String(100), nullable=False)
    commission_percent = Column(Numeric(precision=5, scale=2), nullable=False, server_default='0')
    commission_fixed = Column(Numeric(precision=5, scale=2), nullable=False, server_default='0')
    period = Column(String(30), nullable=False)
    order = Column(Integer, nullable=False)
    schema = Column(JSONB)


class PayoutMethodCountry(Base):
    __tablename__ = 'payout_method_country'

    id = Column(UUID, primary_key=True)
    country = Column(String(2), index=True)

    @declared_attr
    def method_id(cls):
        return Column(UUID, ForeignKey('payout_method.id', ondelete='no action'), nullable=False)


class Payout(Base):
    __tablename__ = 'payout'

    __table_args__ = (
        PrimaryKeyConstraint('id', 'created_at'),
    )

    id = Column(UUID)
    created_at = Column(DateTime, nullable=False, index=True)
    transaction_id = Column(UUID, nullable=False)

    @declared_attr
    def user_id(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=False, index=True)

    status = Column(SmallInteger, nullable=False)
    reason = Column(String, nullable=True)
    amount = Column(Numeric(precision=10, scale=2))
    commission = Column(Numeric(precision=10, scale=2))
    data = Column(JSONB)

    @declared_attr
    def method_id(cls):
        return Column(UUID, ForeignKey('payout_method.id', ondelete='no action'), nullable=False, index=True)

    @declared_attr
    def updated_by(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=True)


class Invite(Base):
    __tablename__ = 'invite'

    id = Column(UUID, primary_key=True)
    slug = Column(String(32), index=True)
    created_at = Column(DateTime, nullable=False)
    expiry_at = Column(DateTime, nullable=False)
    comment = Column(String(100), nullable=True)


class PaymentProfile(Base):
    __tablename__ = 'payment_profile'
    
    id = Column(UUID, primary_key=True)

    @declared_attr
    def user_id(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=False)

    @declared_attr
    def updated_by(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=True)

    updated_at = Column(DateTime)
    status = Column(SmallInteger)
    reason = Column(String, nullable=True)

    first_name = Column(String)
    last_name = Column(String)
    birthday = Column(DateTime)
    country = Column(String(2))
    resident = Column(SmallInteger)
    address = Column(String)
    city = Column(String)
    zip = Column(String)
    state = Column(String)
    tax_id = Column(String)
    document_type = Column(SmallInteger)

    @declared_attr
    def document_front(cls):
        return Column(UUID, ForeignKey('file.id', ondelete='no action'), nullable=True)

    @declared_attr
    def document_back(cls):
        return Column(UUID, ForeignKey('file.id', ondelete='no action'), nullable=True)


class ProfileMedia(Base):
    __tablename__ = 'profile_media'

    id = Column(UUID, primary_key=True)

    @declared_attr
    def user_id(cls):
        return Column(UUID, ForeignKey('user.id', ondelete='no action'), nullable=False)

    @declared_attr
    def file_id(cls):
        return Column(UUID, ForeignKey('file.id', ondelete='no action'), nullable=False)

    order = Column(Integer)
