import io
import imghdr
import asyncio
import tempfile
from datetime import datetime
from typing import IO, Dict, Union, List

import ffmpeg
import aiobotocore
from PIL import Image, ImageOps
from starlette.concurrency import run_in_threadpool

from schemas import Thumbnail
from config import AWS_S3_KEY, AWS_S3_SECRET, AWS_S3_REGION, AWS_S3_BUCKET


__all__ = (
    'save_to_storage',
    'save_thumbnails',
    'create_image_thumbnails',
    'get_info',
    'create_video_thumbnails',
)


async def save_to_storage(file: Union[bytes, bytearray, IO], tag: str, filename: str, content_type: str) -> (str, str):
    session = aiobotocore.get_session()

    service = 's3'

    date = datetime.now().strftime('%Y%m%d')

    key = f'uploads/{date}/{tag}/{filename}'

    async with session.create_client(service, region_name=AWS_S3_REGION, aws_secret_access_key=AWS_S3_SECRET, aws_access_key_id=AWS_S3_KEY) as client:
        response = await client.put_object(
            Bucket=AWS_S3_BUCKET,
            Key=key,
            Body=file,
            ContentType=content_type,
        )

    return tag, f'https://{AWS_S3_BUCKET}.{service}.{AWS_S3_REGION}.amazonaws.com/{key}'


def _create_image_thumbnails(file: IO, sizes: Dict[str, int], image_type: str = None) -> (str, Dict):
    if image_type is None:
        image_type = imghdr.what(file)

    content_type = f'image/{image_type}'

    image = Image.open(file)
    image = ImageOps.exif_transpose(image)

    images = {}

    img = image

    for tag, size in sizes.items():
        img.thumbnail((size, size), Image.ANTIALIAS)

        width, height = img.size

        arr = io.BytesIO()
        img.save(arr, format=image_type)

        data = arr.getvalue()

        images[tag] = {
            'size': len(data),
            'data': data,
            'width': width,
            'height': height,
            'type': image_type,
        }

        img = img.copy()

    return content_type, images


async def create_image_thumbnails(file: IO, sizes: Dict[str, int], image_type: str = None) -> (str, Dict):
    return await run_in_threadpool(_create_image_thumbnails, file, sizes, image_type)


async def save_thumbnails(file_id, images) -> Dict[str, Thumbnail]:
    tasks = []
    thumbnails = {}

    for tag, image in images.items():
        image_type = image['type']
        name = f'{file_id}.{image_type}'
        tasks.append(save_to_storage(image['data'], tag, name, image_type))

    urls = await asyncio.gather(*tasks)

    for tag, url in urls:
        image = images[tag]

        thumbnails[tag] = Thumbnail(
            width=image['width'],
            height=image['height'],
            size=image['size'],
            url=url,
        )

    return thumbnails


def create_video_thumbnail(filename: str, sizes: Dict[str, int]):
    process = (
        ffmpeg
        .input(filename, ss=0)
        .output('pipe:', format='image2', vcodec='mjpeg', vframes=1)
        .run_async(pipe_stdout=True, quiet=True)
    )

    return _create_image_thumbnails(process.stdout, sizes, 'jpeg')


async def create_video_thumbnails(filename, sizes: Dict[str, int]):
    return await run_in_threadpool(create_video_thumbnail, filename, sizes)


async def get_info(filename: str) -> (Dict, List[Dict]):
    info = await run_in_threadpool(ffmpeg.probe, filename)

    return info['format'], info['streams']


def _convert_audio(file: IO, output: str):
    process = (
        ffmpeg
        .input('pipe:')
        .output(output, acodec='libmp3lame', format='mp3', **{'ab': '48k'})
        .run_async(pipe_stdin=True, quiet=True)
    )

    process.communicate(input=file.read())
    process.wait()


async def convert_audio(file: IO, output: str):
    await run_in_threadpool(_convert_audio, file, output)

    return await get_info(output)
